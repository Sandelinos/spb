# Slide(reddit client) porn blocklist

This is a simple bash script that you can run on your slide settings backup file to add 740 porn subreddits to the subreddit filter list.
The blocklist has been compiled from /r/ListOfSubreddits(474 subreddits) and my personal filterlist.

## Usage:
### Linux:
First you need to backup your Slide configuration by going to Settings/Backup & restore and hitting "Backup to file" and move it to your PC.

Then run these commands:

`git clone https://gitlab.com/Sandelinos/spb.git`

`cd spb`

`./spb /path/to/your/Slide-(use tab completion)-personal.txt`

You will see a new file in the folder called "Slide-new.txt", copy it to your phone and load it into Slide with the "Restore from file" option in the Backup settings.

### Android
You can also use Termux to run the script on your phone without the need for a computer.

1. Open Slide, go to Settings/Backup & restore and hit "Backup to file"
2. Install Termux from either [Google Play](https://play.google.com/store/apps/details?id=com.termux) or [F-Droid](https://play.google.com/store/apps/details?id=com.termux)
3. Open Termux, run `termux-setup-storage` and hit yes on the permissions popup
4. Run these commands:

`pkg install git`

`git clone https://gitlab.com/Sandelinos/spb.git`

`cd spb`

`./spb ~/storage/downloads/Slide-(use tab completion)-personal.txt`

5. Go back to Slide and hit on Restore from file
6. Pull out the left hamburger menu in your file manager and click on Termux
7. Open the folder "spb" and select the file "Slide-new.txt"
